import React from 'react';

export default function ButtonSocial({ children }) {
  return (
    <>
      <button className="btn-primary">
        {children}
      </button>
    </>
  )
}