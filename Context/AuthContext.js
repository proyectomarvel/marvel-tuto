import { createContext, useState } from 'react';
import Router from 'next/router';

import firebase from '../firebase/firebase';


const AuthContext = createContext();

export function AuthProvider({ children }) {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  const signinGoogle = async () => {
    try {
      setLoading(true);
      return await firebase
      .auth()
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((response) => {
        setUser(response.user);
        Router.push('/');
      });
    } finally {
      setLoading(false);
    }
  };

  const signinFacebook = async () => {
    try {
        setLoading(true);
        const response = await firebase
            .auth()
            .signInWithPopup(new firebase.auth.FacebookAuthProvider());
            setUser(response.user);
            Router.push('/')
            console.log("facebook", user)
    } finally {
        setLoading(false);
    }
}

  const sendPasswordResetEmail = (email) => {
    return firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        return true;
      });
  };
  const confirmPasswordReset = (code, password) => {
    return firebase
      .auth()
      .confirmPasswordReset(code, password)
      .then(() => {
        return true;
      });
  };

  const signout = async () => {
    try {
      Router.push('/');
      await firebase.auth().signOut();
      handleUser(false);

    } finally {
      setLoading(false);
    }
  };


  return <AuthContext.Provider value={{
    user,
    loading,
    signinGoogle,
    signinFacebook,
    signout
  }}>{children}</AuthContext.Provider>
}

export const AuthConsumer = AuthContext.Consumer;

export default AuthContext;