import React, { useState } from 'react';
import Start from '../Components/Start';
import ButtonSocial from '../Components/ButtonSocial';
import Google from '../Components/Icons/Google';
import Facebook from '../Components/Icons/Facebook';

import useAuth from '../hooks/useAuth';

export default function login(props) {

    const { user, signinGoogle, signinFacebook } = useAuth();
    console.log("Google", user)

    return (
        <>
            <div className="contenedor">
                <Start />
                <div className="content">
                    <h2>Sign up</h2>
                    <div className="content__btn">
                        <button className="btn-primary" onClick={() => signinGoogle()}>
                            <Google/>
                            Sign In with Google
                        </button>

                        <button className="btn-primary" onClick={() => signinFacebook()}>
                            <Facebook/>
                            Sign up with Facebook
                        </button>
                    </div>
                    <div className="content__line">
                        <div className="content__line--left"></div>
                        <div className="content__line--or">OR</div>
                        <div className="content__line--right"></div>
                    </div>
                    <form className="content__form">
                        <label className="name">
                            <span>First Name</span>
                            <input type="text" placeholder="First Name"/>
                        </label>
                        <label className="lastname">
                            <span>Last Name</span>
                            <input type="text" placeholder="Last Name"/>
                        </label>
                        <label className="email">
                            <span>Email</span>
                            <input type="email" placeholder="email"/>
                        </label>
                        <label className="password">
                            <span>Password</span>
                            <input type="password" placeholder="Password"/>
                        </label>
                        <div>
                            <ButtonSocial type="submit">
                                Sign up to your account
                            </ButtonSocial>
                        </div>
                    </form>

                </div>
            </div>
        </>
    )
}

