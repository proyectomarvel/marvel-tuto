import Head from 'next/head'
import NavBar from '../Components/NavBar'
import Footer from '../Components/Footer'

export default function Layout({ children }) {
    return (
        <div>
            <Head>
                <meta
                    name="description"
                    content="Marvel Catalog"
                />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <title>Marvel Catalog</title>
            </Head>
            
           <NavBar />
            <main>
                {children}
                

            </main>
            {/* Únete */}
           <Footer />
        </div>
        
    )
}